package day4.calendars;

import homework.day4.deniscalendars.Gregorian;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GregorianTest {


    @Test
     void addDay_1() throws IllegalArgumentException{
        try{
            Gregorian gregorian = new Gregorian(-122,10,1990);
            gregorian.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_2() throws IllegalArgumentException{
        try{
            Gregorian gregorian = new Gregorian(31,6,1990);
            gregorian.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_3() throws IllegalArgumentException{
        try{
            Gregorian gregorian = new Gregorian(28,11,0);
            gregorian.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_4() {
        Gregorian gregorian = new Gregorian(30,4,2000);
        gregorian.addDay();
        assert gregorian.getDay() == 1;
    }

    @Test
    void addDay_5() {
        Gregorian gregorian = new Gregorian(31,12,2000);
        gregorian.addDay();
        assert gregorian.getDay() == 1;
        assert gregorian.getMonth() == 1;
        assert gregorian.getYear() == 2001;
    }

    @Test
    void addDay_6() {
        Gregorian gregorian = new Gregorian(28,2,2020);
        gregorian.addDay();
        assert gregorian.getDay() == 1;
        assert gregorian.getMonth() == 3;
    }

    @Test
    void getString_1() {
        Gregorian gregorian = new Gregorian(20,3,2021);
        assert "20 March 2021".equals(gregorian.getString());
    }

    @Test
    void getString_2() {
        Gregorian gregorian = new Gregorian(10,4,2018);
        assert (gregorian.getDay()+" "+gregorian.monthesString[gregorian.getMonth() - 1]+" "+gregorian.getYear()).equals(gregorian.getString());
    }
}