package day4.calendars;

import homework.day4.deniscalendars.Hijra;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HijraTest {

    @Test
    void addDay_1() throws IllegalArgumentException{
        try{
            Hijra hijra = new Hijra(30,6,1400);
            hijra.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_2() throws IllegalArgumentException{
        try{
            Hijra hijra = new Hijra(27,13,1230);
            hijra.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_3() throws IllegalArgumentException{
        try{
            Hijra hijra = new Hijra(10,10,0);
            hijra.addDay();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void addDay_4() {
        Hijra hijra = new Hijra(10,4,1500);
        hijra.addDay();
        assert hijra.getDay() == 11;
    }

    @Test
    void addDay_5() {
        Hijra hijra = new Hijra(29,12,1450);
        hijra.addDay();
        assert hijra.getDay() == 1;
        assert hijra.getMonth() == 1;
        assert hijra.getYear() == 1451;
    }

    @Test
    void getString_1() {
        Hijra hijra = new Hijra(20,1,2021);
        assert "20 Muharram 2021".equals(hijra.getString());
    }

    @Test
    void getString_2() {
        Hijra hijra = new Hijra(10,4,2018);
        assert (hijra.getDay()+" "+hijra.monthesString[hijra.getMonth() - 1]+" "+hijra.getYear()).equals(hijra.getString());
    }
}