package day4.figures;

import homework.day4.OlegFigures.Circle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    @Test
    void getPerimeter_1() {
        try{
            Circle circle = new Circle(2,1,0);
            circle.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_2() {
        Circle circle = new Circle(2,1,3);
        assert 9 == (int)circle.getPerimeter();
    }

    @Test
    void getPerimeter_3() {
        Circle circle = new Circle(2,1,2.5);
        assert 7 == (int)circle.getPerimeter();
    }

    @Test
    void move_1() {
        Circle circle = new Circle(-7,0,2.5);
        double tmpX = circle.getX();
        double tmpY = circle.getY();
        circle.move(-1, -1);
        assert circle.getX() == tmpX - 1;
        assert circle.getY() == tmpY - 1;
    }

    @Test
    void move_2() {
        Circle circle = new Circle(0,4,2.5);
        double tmpX = circle.getX();
        double tmpY = circle.getY();
        double tmpPerimeter = circle.getPerimeter();
        circle.move(2, 8);
        assert circle.getX() == tmpX + 2;
        assert circle.getY() == tmpY + 8;
        assert tmpPerimeter == circle.getPerimeter();
    }
}