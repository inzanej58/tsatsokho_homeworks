package day4.figures;

import homework.day4.OlegFigures.Square;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test
    void getPerimeter_1() {
        try{
            Square square = new Square(2,1,0);
            square.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_2() {
        Square square = new Square(2,1,4.1);
        assert 16 == (int)square.getPerimeter();
    }

    @Test
    void getPerimeter_3() {
        Square square = new Square(2,1,3.2);
        assert 12.8 == square.getPerimeter();
    }

    @Test
    void move_1() {
        Square square = new Square(2,1,3.2);
        double tmpX = square.getX();
        double tmpY = square.getY();
        square.move(0, -1);
        assert square.getX() == tmpX;
        assert square.getY() == tmpY - 1;
    }

    @Test
    void move_2() {
        Square square = new Square(-2,0,2.2);
        double tmpX = square.getX();
        double tmpY = square.getY();
        double tmpPerimeter = square.getPerimeter();
        square.move(-1, 2);
        assert square.getX() == tmpX - 1;
        assert square.getY() == tmpY + 2;
        assert tmpPerimeter == square.getPerimeter();
    }
}