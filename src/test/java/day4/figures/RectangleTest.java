package day4.figures;

import homework.day4.OlegFigures.Rectangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    @Test
    void getPerimeter_1() {
        try{
            Rectangle rectangle = new Rectangle(2,1,2, 0);
            rectangle.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_2() {
        try{
            Rectangle rectangle = new Rectangle(2,1,-3, 1);
            rectangle.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_3() {
        try{
            Rectangle rectangle = new Rectangle(2,1,0);
            rectangle.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_4() {
        Rectangle rectangle = new Rectangle(2,1,4, 5);
        assert 18 == (int)rectangle.getPerimeter();
    }

    @Test
    void getPerimeter_5() {
        Rectangle rectangle = new Rectangle(2,1,0.5, 1);
        assert 3.0 == rectangle.getPerimeter();
    }
}