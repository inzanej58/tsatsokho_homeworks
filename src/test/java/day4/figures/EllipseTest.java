package day4.figures;

import homework.day4.OlegFigures.Ellipse;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EllipseTest {

    @Test
    void getPerimeter_1() {
        try{
            Ellipse ellipse = new Ellipse(2,1,2, 0);
            ellipse.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_2() {
        try{
            Ellipse ellipse = new Ellipse(2,1,-3, 1);
            ellipse.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_3() {
        try{
            Ellipse ellipse = new Ellipse(2,1,0);
            ellipse.getPerimeter();
        } catch (IllegalArgumentException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }
    }

    @Test
    void getPerimeter_4() {
        Ellipse ellipse = new Ellipse(2,1,25, 10);
        assert 59 == (int)ellipse.getPerimeter();
    }

    @Test
    void getPerimeter_5() {
        Ellipse ellipse = new Ellipse(2,1,3.5, 2.5);
        assert 9 == (int)ellipse.getPerimeter();
    }
}