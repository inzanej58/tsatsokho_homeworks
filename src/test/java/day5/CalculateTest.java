package day5;

import homework.day5.static_hw.Calculate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculateTest {

    @Test
    void sum_1() {
        assert 8 == Calculate.sum(-5,2,0,1,10);
    }

    @Test
    void sum_2() {
        assert 0 == Calculate.sum(-5441, 5441);
    }


    @Test
    void sub_1() {
        assert 135 == Calculate.sub(-122,0,-12,-1);
    }

    @Test
    void sub_2() {
        assert -9 == Calculate.sub(15,15,2,7);
    }

    @Test
    void multi_1() {
        assert 0 == Calculate.multi(10,4,3,2,0);
    }

    @Test
    void multi_2() {
        assert -24 == Calculate.multi(-1,2,3,4);
    }

    @Test
    void div_1() throws ArithmeticException {
        try{
            Calculate.div(40,0,2);
        } catch (ArithmeticException thrown){
            assertNotEquals("", thrown.getMessage());
            System.out.println(thrown.getMessage());
        }

    }

    @Test
    void div_2() {
        assert 0 == Calculate.div(5,10,2);
    }

    @Test
    void div_3() {
        assert 2 == Calculate.div(10,5,4);
    }

    @Test
    void factorial_1() {
        assert 6 == Calculate.factorial(3);
    }

    @Test
    void factorial_2() {
        assert 1 == Calculate.factorial(-25);
    }
}