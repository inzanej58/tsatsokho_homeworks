package day2;

import homework.day2.DemoApp;
import org.junit.jupiter.api.Test;

class DemoAppTest {
    @Test
    void checkIfOdd_1() {
        DemoApp app = new DemoApp();
        assert app.checkIfOdd(20) == false;
    }

    @Test
    void checkIfOdd_2() {
        DemoApp app = new DemoApp();
        assert app.checkIfOdd(23) == true;
    }
}