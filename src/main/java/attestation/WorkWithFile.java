package attestation;

import java.util.List;
import java.util.Map;

public interface WorkWithFile {

    List<String> fileToStringList(String path);

    Map<String, Integer> countWords(String path);

    Map<Character, Integer> countLetters(String path);

    void writeWordsOnFile(Map<String, Integer> map);

    void writeLettersOnFile(Map<Character, Integer> map);
}
