package attestation;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WorkWithFileImpl implements WorkWithFile{
    @Override
    public List<String> fileToStringList(String path) {
        StringBuilder builder = new StringBuilder();
        try (Scanner s = new Scanner(new File(path))) {
            while (s.hasNext()) {
                String newLine = s.next();
                builder.append(newLine);
                builder.append(" ");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String text = builder.toString();
        List<String> words = Stream.of(text.split(" "))
                .filter(f -> f != null)
                .filter(f -> f.length() != 0)
                .map(f -> f.toLowerCase(Locale.ROOT))
                .map(f -> removeSpecialMarks(f))
                .collect(Collectors.toList());

        return words;
    }

    @Override
    public Map<String, Integer> countWords(String path) {
        List<String> allWords = fileToStringList(path);
        Map<String, Integer> freq = new TreeMap<>();
        for (String s : allWords) {
            freq.putIfAbsent(s, 0);
            Integer currFreq = freq.get(s);
            freq.put(s, currFreq + 1);
        }

        return freq;
    }

    @Override
    public Map<Character, Integer> countLetters(String path) {
        List<Character> letters = fileToStringList(path).stream()
                        .flatMapToInt(String::chars)
                        .mapToObj(f -> (char) f)
                        .collect(Collectors.toList());
        Map<Character, Integer> freq = new TreeMap<>();
        for (Character c : letters) {
            freq.putIfAbsent(c, 0);
            Integer currFreq = freq.get(c);
            freq.put(c, currFreq + 1);
        }
        return freq;
    }

    @Override
    public void writeWordsOnFile(Map<String, Integer> map) {
        File file = new File("c:/countWords.txt");
        try (Writer writer = new FileWriter(file)) {
            for (Map.Entry<String, Integer> pair : map.entrySet()) {
                writer.write(pair.getKey() + " - " + pair.getValue());
                writer.write("\n");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void writeLettersOnFile(Map<Character, Integer> map) {
        File file = new File("c:/countLetters.txt");
        try (Writer writer = new FileWriter(file)) {
            for (Map.Entry<Character, Integer> pair : map.entrySet()) {
                writer.write(pair.getKey() + " - " + pair.getValue());
                writer.write("\n");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String removeSpecialMarks(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        s = s.replace(".", "");
        s = s.replace(",", "");
        s = s.replace("!", "");
        s = s.replace("?", "");
        s = s.replace("+", "");
        s = s.replace("(", "");
        s = s.replace(")", "");
        s = s.replace("\"\"", "");
        s = s.replace("«", "");
        s = s.replace("»", "");
        s = s.replace(":", "");
        s = s.replace("—", "");
        s = s.replace("-", "");

        return s;
    }

}