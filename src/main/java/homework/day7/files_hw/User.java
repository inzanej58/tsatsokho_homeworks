package homework.day7.files_hw;

import java.util.Objects;
import java.util.Random;

public class User {
    private int id;
    private String name;
    private String lastname;
    private int age;
    private boolean job;

    public User(String name, String lastname, int age, boolean job) {
        this.id = new Random().nextInt(1000);
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.job = job;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
            return id;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isJob() {
        return job;
    }

    public void setJob(boolean job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + lastname + "|" + age + "|" + job;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && age == user.age && job == user.job && Objects.equals(name, user.name) && Objects.equals(lastname, user.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastname, age, job);
    }
}
