package homework.day7.files_hw;

public class Main {
    public static void main(String[] args) {

        User ivan = new User("Ivan","Ivanov", 40, false);
        User petr = new User("Petr","Petrov", 60, true);
        User sergey = new User("Sergey","Sergeev", 20, true);
        User andrey = new User("Andrey","Andreev", 27, false);
        User anton = new User("Anton","Antonov", 36, true);

        UsersRepositoryFileImpl rep = new UsersRepositoryFileImpl();

        rep.create(ivan);
        rep.create(petr);
        rep.create(sergey);
        rep.create(andrey);
        rep.create(anton);

        User unknown = rep.findById(petr.getId());
        unknown.setName("Vasily");
        unknown.setAge(47);
        rep.update(unknown);

        rep.delete(sergey.getId());


    }
}
