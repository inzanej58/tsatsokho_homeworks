package homework.day7.files_hw;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class UsersRepositoryFileImpl implements UsersRepositoryFile{
    Map<Integer, User> map = new HashMap<>();
    File file = new File("c:/users.txt");

    @Override
    public User findById(int id)
    {
        if (map.get(id) == null){
            System.out.println("User does not exist");
        }
        return map.get(id);
    }

    @Override
    public void create(User user) {
        try (Writer writer = new FileWriter(file, true)) {
            map.putIfAbsent(user.getId(), user);
            writer.write(user.toString());
            writer.write("\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(User user) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                int strId = Integer.parseInt(info[0]);
                String name = info[1];
                String lastname = info[2];
                int age = Integer.parseInt(info[3]);
                boolean job = Boolean.getBoolean(info[4]);

                if (strId == user.getId()){
                    map.put(user.getId(), user);
                    try (Writer writer = new FileWriter(file, false)) {
                        for (Integer key : map.keySet()) {
                            writer.write(map.get(key).toString());
                            writer.write("\n");
                        }
                    } catch (IOException exception) {
                        throw new RuntimeException(exception);
                    }
                }
            }

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void delete(int id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                int strId = Integer.parseInt(info[0]);
                String name = info[1];
                String lastname = info[2];
                int age = Integer.parseInt(info[3]);
                boolean job = Boolean.getBoolean(info[4]);

                if (strId == id){
                    map.remove(id);
                    try (Writer writer = new FileWriter(file, false)) {
                        for (Integer key : map.keySet()) {
                            writer.write(map.get(key).toString());
                            writer.write("\n");
                        }
                    } catch (IOException exception) {
                        throw new RuntimeException(exception);
                    }
                }
            }

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
