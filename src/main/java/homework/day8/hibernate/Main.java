package homework.day8.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            Teacher teacher1 = Teacher.builder()
                    .name("Ivan")
                    .lastname("Ivanov")
                    .build();

            Teacher teacher2 = Teacher.builder()
                    .name("Petr")
                    .lastname("Petrov")
                    .build();

            Student student1 = Student.builder()
                    .name("Anton")
                    .lastname("Antonov")
                    .build();

            Student student2 = Student.builder()
                    .name("Sergei")
                    .lastname("Sergeev")
                    .build();

            Student student3 = Student.builder()
                    .name("Andrey")
                    .lastname("Andreev")
                    .build();

            Set<Teacher> teachers = new HashSet<>();
            Set<Student> students = new HashSet<>();

            teachers.add(teacher1);
            teachers.add(teacher2);

            students.add(student1);
            students.add(student2);
            students.add(student3);

            teacher1.setStudents(students);
            teacher2.setStudents(students);

            student1.setTeachers(teachers);
            student2.setTeachers(teachers);
            student3.setTeachers(teachers);

            session.save(teacher1);
            session.save(teacher2);
            session.save(student1);
            session.save(student2);
            session.save(student3);

            transaction.commit();


        }

    }

}
