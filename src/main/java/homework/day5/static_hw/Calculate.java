package homework.day5.static_hw;

public class Calculate {

    public static int sum(int... numbers){
        int result = 0;
        for (int number : numbers) {
            result = result + number;
        }
        return result;
    }

    public static int sub(int... numbers){
        int max = 0;
        int tmp;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max){
                tmp = max;
                max = numbers[i];
                numbers[i] = tmp;
            }
        }

        for (int number : numbers) {
            max = max - number;
        }
        return max;
    }

    public static int multi(int... numbers){
        int result = 1;
        for (int number : numbers) {
            result = result * number;
        }
        return result;
    }

    public static int div(int... numbers) {
        int result = 0;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i-1] >= numbers[i] && numbers[i] > 0){
                result = numbers[i-1] / numbers [i];
                numbers[i] = result;
            } else if (numbers[i] == 0) {
                throw new ArithmeticException("Error: division by zero");
            } else{
                break;
            }
        }
        return result;
    }

    public static int factorial(int num) {
        if (num <= 1) {
            return 1;
        }
        else {
            return num * factorial(num - 1);
        }
    }
}
