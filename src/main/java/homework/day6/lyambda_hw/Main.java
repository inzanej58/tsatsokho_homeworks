package homework.day6.lyambda_hw;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {12321,231,818,-41,60,222,-17,0,9};

        //checking the even number
        ByCondition even = number -> number % 2 == 0;

        //checking the even sum of the digits of the number
        ByCondition evenSumDigits = number -> {
            int sum = 0;
            while(number != 0){
                sum += (number % 10);
                number /= 10;
            }
            return sum % 2 == 0;
        };

        //checking the even digits of the number
        ByCondition evenDigits = number -> {
            boolean isTrue = false;
            while(number != 0)
            {
                int tmp = number % 10;
                isTrue = tmp % 2 == 0;
                if (isTrue){
                    number /= 10;
                } else {
                    break;
                }
            }
            return isTrue;
        };

        //checking a number for a palindrome
        ByCondition palindrome = number -> {
            int palind = number;
            int reverse = 0;

            while(palind != 0)
            {
                int tmp = palind % 10;
                reverse = (reverse * 10) + tmp;
                palind /= 10;
            }
            return reverse == number;
        };

        System.out.println("Even numbers in array: " + Arrays.toString(Sequence.filter(array, even)));
        System.out.println("Numbers with even sum of the digits: " + Arrays.toString(Sequence.filter(array, evenSumDigits)));
        System.out.println("Numbers with ALL even digits: " + Arrays.toString(Sequence.filter(array, evenDigits)));
        System.out.println("Palindrome numbers: " + Arrays.toString(Sequence.filter(array, palindrome)));

    }
}
