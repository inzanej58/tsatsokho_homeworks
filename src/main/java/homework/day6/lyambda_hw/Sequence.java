package homework.day6.lyambda_hw;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int j = 0;
        int count = 0;

        for (int k : array) {
            if (k == 0) {
                continue;
            }
            if (condition.isOk(k)) {
                count++;
            }
        }

        int[] result = new int[count];

        for (int k : array) {
            if (k == 0) {
                continue;
            }
            if (condition.isOk(k)) {
                result[j] = k;
                j++;
            }
        }
        return result;

    }


}
