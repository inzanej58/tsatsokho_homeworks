package homework.day6.lyambda_hw;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);
}
