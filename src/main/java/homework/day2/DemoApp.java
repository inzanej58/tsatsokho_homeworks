package homework.day2;

public class DemoApp {
    public static void main(String[] args) {
        DemoApp theApp = new DemoApp();
        theApp.run();
    }

    public void run() {
        //main appliaction startup point
        System.out.println("20 - is odd " + checkIfOdd(20));
        System.out.println("23 - is odd " + checkIfOdd(23));
    }

    public boolean checkIfOdd(int param) {
        return param % 2 != 0;
    }
}
