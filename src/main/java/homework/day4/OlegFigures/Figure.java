package homework.day4.OlegFigures;

public abstract class Figure {
    double x;
    double y;


    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public abstract double getPerimeter();
}
