package homework.day4.OlegFigures;

public class Square extends Rectangle implements Movable{

    public Square(double x, double y, double side) {
        super(x, y, side);
    }

    @Override
    public double getPerimeter() {
        return 4 * side;
    }

    @Override
    public void move(int x, int y) {
        this.x = getX() + x;
        this.y = getY() + y;
    }
}
