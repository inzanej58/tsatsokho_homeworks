package homework.day4.OlegFigures;

public class Circle extends Ellipse implements Movable{

    public Circle(double x, double y, double diameter) {
        super(x, y, diameter);
    }
    @Override
    public double getPerimeter() {
        return Math.PI * diameter;
    }

    @Override
    public void move(int x, int y) {
        this.x = getX() + x;
        this.y = getY() + y;
    }
}
