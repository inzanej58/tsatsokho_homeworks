package homework.day4.OlegFigures;

public class Rectangle extends Figure{
    double width;
    double length;
    double side;

    public Rectangle(double x, double y, double width, double length) {
        super(x, y);
        if (width <= 0 || length <= 0){
            throw new IllegalArgumentException("Invalid argument. Width or length must be more than 0");
        }
        this.width = width;
        this.length = length;
    }

    public Rectangle(double x, double y, double side) {
        super(x, y);
        if (side <= 0){
            throw new IllegalArgumentException("Invalid argument. Side must be more than 0");
        }
        this.side = side;
    }

    @Override
    public double getPerimeter() {
        return 2 * (width + length);
    }
}
