package homework.day4.OlegFigures;

public interface Movable {
    void move (int x, int y);
}
