package homework.day4.OlegFigures;

public class Ellipse extends Figure{
    double longAxis;
    double shortAxis;
    double diameter;
    public Ellipse(double x, double y, double longAxis, double shortAxis)
    {
        super(x, y);
        if (longAxis <= 0 || shortAxis <= 0){
            throw new IllegalArgumentException("Invalid argument. longAxis or shortAxis must be more than 0");
        }
        this.longAxis = longAxis;
        this.shortAxis = shortAxis;

    }

    public Ellipse(double x, double y, double diameter) {
        super(x, y);
        if (diameter <= 0){
            throw new IllegalArgumentException("Invalid argument. diameter must be more than 0");
        }
        this.diameter = diameter;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(longAxis, 2) + Math.pow(shortAxis, 2)) / 8);
    }
}
