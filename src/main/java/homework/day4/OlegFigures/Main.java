package homework.day4.OlegFigures;

public class Main {
    //������� ����������� ����� Figure, � ������� ������ ���� ��� ���� - x � y ����������.
    //������ Ellipse � Rectangle ������ ���� ��������� ������ Figure.
    //����� Square - ������� ������ Rectangle, Circle - ������� ������ Ellipse.
    //� ������ Figure ������������� ����������� ����� getPerimeter().
    //��� ��, ����� ���������� ��������� Moveable c ������������ ������� .move(int x, int y),
    // ������� �������� ���������� ������ �� �������� ����������.
    //������ ��������� ������ ����������� ������ ������ Circle � Square.
    //� Main ������� ������ ���� ����� � "������������" �����. � ���� ������� � ������� ��������,
    // � � "������������" ����� �������� ��������� ������� ����������.
    public static void main(String[] args) {
        Figure[] figures = {
                new Ellipse(5,7,2,1),
                new Rectangle(3.5, 2,4,2),
                new Square(2, 5.8,5),
                new Circle(3.5, 5,3)};

        for (Figure figure : figures) {
            System.out.print("�������� ������ �����: ");
            System.out.println(figure.getPerimeter());
        }

        Square square = new Square(2, 5.8,5);
        System.out.println("��������� ���������� ��������: " + square.getX() + " " + square.getY());
        Circle circle = new Circle(3.5, 5,3);
        System.out.println("��������� ���������� �����: " + circle.getX() + " " + circle.getY());

        square.move(4,2);
        System.out.println("���������� �������� ����� �����������: " + square.getX() + " " + square.getY());
        circle.move(1,1);
        System.out.println("���������� ����� ����� �����������: " + circle.getX() + " " + circle.getY());




    }
}
