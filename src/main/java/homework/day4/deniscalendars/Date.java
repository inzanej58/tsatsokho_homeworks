package homework.day4.deniscalendars;

public interface Date {
    //интерфейс Date, с 2я методами addDay() который будет прибавлять 1 день
    //и getString() который будет возвращать например, 16 August 2022

    void addDay();

    String getString();
}
