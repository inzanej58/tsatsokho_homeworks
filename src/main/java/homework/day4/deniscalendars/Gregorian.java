package homework.day4.deniscalendars;


public class Gregorian implements Date {

    private int day;
    private int month;
    private int year;
    public String[] monthesString = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        if (day < 1 || day > 31) {
            throw new IllegalArgumentException(day +
                    " is not a valid day. Day must be between 1 and 31 inclusive");
        }
        this.day = day;
    }

    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException(month +
                    " is not a valid month. Month must be between 1 and 12 inclusive");
        } else if (month == 2 && day > 28){
            throw new IllegalArgumentException(day +
                    " is not a valid day for " + monthesString[month - 1] +
                    ". Day must be between 1 and 28 inclusive");
        } else if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30){
            throw new IllegalArgumentException(day +
                    " is not a valid day for " + monthesString[month - 1] +
                    ". Day must be between 1 and 30 inclusive");
        }
        this.month = month;
    }

    public void setYear(int year) {
        if (year < 1) {
            throw new IllegalArgumentException(year +
                    " is not a valid year. Year must be more than 0");
        }
        this.year = year;
    }

    public Gregorian(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    @Override
    public void addDay() {
            if (month == 2){
                if (day == 28){
                    day = 1;
                    month++;
                } else {
                    day += 1;
                }
            } else if (month == 4 || month == 6 || month == 9 || month == 11){
                if (day == 30){
                    day = 1;
                    month += 1;
                } else {
                    day += 1;
                }
            } else if (day == 31 && month == 12){
                day = 1;
                month = 1;
                year += 1;
            } else {
                if (day == 31){
                    day = 1;
                    month += 1;
                } else {
                    day += 1;
                }
            }

    }

    @Override
    public String getString() {
        return day + " " + monthesString[month - 1] + " " + year;
    }
}
