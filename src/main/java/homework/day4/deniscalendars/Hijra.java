package homework.day4.deniscalendars;



public class Hijra implements Date {
    private int day;
    private int month;
    private int year;

    public String[] monthesString = {
            "Muharram",
            "Safar",
            "Rabi Al-Awwal",
            "Rabi Al-Thani",
            "Jumada Al-Awwal",
            "Jumada Al-Thani",
            "Rajab",
            "Sha`ban",
            "Ramadan",
            "Shawwal",
            "Dhul-Qa`dah",
            "Dhul-Hijjah"
    };

    public Hijra(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if ((month % 2 == 0) && day > 29){
            throw new IllegalArgumentException(day + " is not a valid day. Day must be between 1 and 29 inclusive in even month");
        } else if ((month % 2 != 0) && day > 30){
            throw new IllegalArgumentException(day + " is not a valid day. Day must be between 1 and 30 inclusive in odd month");
        }
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException(month + " is not a valid month. Month must be between 1 and 12 inclusive");
        }
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year < 1) {
            throw new IllegalArgumentException(year + " is not a valid year. Year must be more than 0");
        }
        this.year = year;
    }



    @Override
    public void addDay() {
        if (day == 29 && month == 12){
            day = 1;
            month = 1;
            year += 1;
        } else if (month % 2 != 0 && day == 30){
            day = 1;
            month += 1;
        } else if (month % 2 == 0 && day == 29) {
            day = 1;
            month += 1;
        } else {
            day += 1;
        }
    }

    @Override
    public String getString() {
        return day + " " + monthesString[month - 1] + " " + year;
    }
}
