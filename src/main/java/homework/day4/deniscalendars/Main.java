package homework.day4.deniscalendars;

public class Main {
    public static void main(String[] args) {
        Gregorian gregorian = new Gregorian(30, 10, 2022);
        System.out.println(gregorian.getString());
        gregorian.addDay();
        System.out.println(gregorian.getString());

        Hijra hijra = new Hijra(29, 12, 1400);
        System.out.println(hijra.getString());
        hijra.addDay();
        System.out.println(hijra.getString());
    }
}
